#!/usr/bin/env bash

set -Eeuo pipefail

RUN_INTERVAL=${RUN_INTERVAL:-60}  # The interval in seconds to wait between checks

##
# function _checkLocalLeader
# This function checks if the local node is a leader in docker swarm
#
# Assumes that the local hostname is seen when running "docker node ls"
#
# Returns:
# 0    if the local node is a leader
# 1    if the local node isn't leader
##
function _checkLocalLeader(){
  if ! docker node ls --filter "name=$(hostname)" --format '{{.ManagerStatus}}' | grep Leader > /dev/null 2>&1; then
    echo "Local node not Leader"
    return 1
  fi
  return 0
}

if [ $# -le 1 ]; then
    echo "Usage $(basename "$0") FILE_NAME COMMAND [ARGS]"
    echo ""
    echo "No params provided. Exiting!"
    exit 1
fi

if /bin/pidof -o %PPID -x "${0##*/}"; then
  echo "Already running. Doing nothing."
  exit 0
fi

SHA="$(sha512sum "${1}")"
NEW_SHA=""

while true; do
  NEW_SHA="$(sha512sum "${1}")"
  if [[ ! "${SHA}x" == "${NEW_SHA}x" ]]; then
    if _checkLocalLeader; then
      echo "Contents of ${1} changed!"
      SHA="${NEW_SHA}"
      set -x
      eval "${@:2}"
      set +x
    else
      SHA="${NEW_SHA}"
    fi
  fi
  sleep "${RUN_INTERVAL}"
done
