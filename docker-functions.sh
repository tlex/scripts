#!/usr/bin/env bash

##
# function _checkLocalLeader
# This function checks if the local node is a leader in docker swarm
#
# Assumes that the local hostname is seen when running "docker node ls"
#
# Returns:
# 0    if the local node is a leader
# 1    if the local node isn't leader
##
function _checkLocalLeader(){
  if ! docker node ls --filter "name=$(hostname)" --format '{{.ManagerStatus}}' | grep Leader > /dev/null 2>&1; then
    echo "Local node not Leader"
    return 1
  fi
  return 0
}

##
# function _checkSwarmMaster
# This function checks if the local node is a master in docker swarm
#
# Returns:
# 0    if the local node is a swarm master
# 1    if the local node isn't a swarm master
##
function _checkSwarmMaster(){
  if ! docker node ls > /dev/null 2>&1; then
    echo "Local node not swarm manager"
    return 1
  fi
  return 0
}
