# Scripts

A multi-purpose script collection

## Scripts
### [execute-in-service.sh](execute-in-service.sh)
This script allows you, on docker swarm, to run a command inside the container of a running service.

It first checks that there is a service container running locally.

Usage:
```sh
execute-in-service.sh SERVICE_NAME COMMAND [ARGS]
```

Use the environment variable `EXEC_OPTS` (default: `-it`) to control the options. See `docker exec --help` for details.


**WARNING**: It will probably fail if you have multiple containers (hint: `replicas` > 1) running on the same swarm
node.

### [file-change-run-on-leader.sh](file-change-run-on-leader.sh)
If you want to run a command on the docker swarm leader node, in case a file gets changed, this script allows you do do
that.

It does that by leveraging `sha512sum`: it compares every `RUN_INTERVAL` seconds (default `60`) the sha512sum of the
given file. Means, the trigger is only contents change, not metadata (timestamp, owner, etc) changes.

Usage:
```sh
file-change-run-on-leader.sh FILE_NAME COMMAND [ARGS]
```

Optionally, set the `RUN_INTERVAL` to another value:
```sh
RUN_INTERVAL=5 file-change-run-on-leader.sh FILE_NAME COMMAND [ARGS]
```

Example:
```sh
file-change-run-on-leader.sh /var/docker/acme.json docker service update --force traefik_traefik
```
