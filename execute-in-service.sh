#!/usr/bin/env bash

set -Eeuo pipefail

EXEC_OPTS=${EXEC_OPTS:--it}

##
# function _checkServiceLocalContainer
# This function checks if there's a local container running for a docker swarm service
#
# Returns:
# 0    if at least one local container exists
# 1    if no local container for the service exists
##
function _checkServiceLocalContainer(){
  local LOCAL_CONTAINER

  LOCAL_CONTAINER=$(docker ps -f "label=com.docker.swarm.service.name=${1}" --format '{{.ID}}' 2>/dev/null)

  if [[ -z "${LOCAL_CONTAINER}" ]]; then
    return 1  # the container is not here
  fi

  echo "${LOCAL_CONTAINER}"
  return 0
}

if [ $# -le 1 ]; then
  echo "Usage $(basename "$0") SERVICE_NAME COMMAND [ARGS]"
  echo ""
  echo "Use the environment variable 'EXEC_OPTS' (default: '-it') to control the options."
  echo "See 'docker exec --help' for details."
  echo ""
  echo "No params provided. Exiting!"
  exit 1
fi

if /bin/pidof -o %PPID -x "${0##*/}"; then
  echo "Already running. Doing nothing."
  exit 0
fi

LOCAL_CONTAINER=$(_checkServiceLocalContainer ${1}) || true
[[ "${LOCAL_CONTAINER}x" == "x" ]] && echo "Service '${1}' not found. Exiting." && exit 0

set -x
docker exec ${EXEC_OPTS} "${LOCAL_CONTAINER}" "${@:2}"
