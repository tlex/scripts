#!/bin/sh

find * -type f -name '*.sh' -exec sha512sum {} ';'|sort > scripts.hash
